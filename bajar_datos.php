<?php
  ini_set('display_errors', 'Off');
  set_time_limit(1200);
  require('consultas.php');

  for($i = 1; $i <= 20; $i++){
    $ch = curl_init('https://api.goplaceit.com/api/v4/search/list?country=cl&platform=web&s[id_modalidad]=2&s[tipo_propiedad][]=1,2&s[sort]=newest&s[center][lng]=-70.65028&s[center][lat]=-33.43778&s[zoom]=10&s[id_moneda_venta]=2&s[id_moneda_arriendo]=1&s[id_tipo_poligono]=1&s[in_viewport]=-33.7049555893916,-71.29160690429612|-33.16977919546218,-70.00895309570265&page=' . $i);
    // $cookie = "C:\\xampp\\htdocs\\Git\\get_toctoc\\cookieTT.txt";
    $cookie = '/var/www/html/generico/goplaceit/cookieGP.txt';

    //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
    curl_setopt ($ch, CURLOPT_POST, false);

    //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0");

    //recogemos la respuesta
    $respuesta = curl_exec($ch);

    if(curl_errno($ch)){
        throw new Exception(curl_error($ch));
    }

    $respuesta = ((array)((array)json_decode($respuesta))['response'])['ids'];

    curl_close($ch);

    for($j = 0; $j < count($respuesta); $j++){
      $chequeo = consultaUnidad('GEO' . $respuesta[$j]);
      if($chequeo[0] >= 1){
        $ch2 = curl_init('https://api.goplaceit.com/api/v4/properties/' . $respuesta[$j]);
        // $cookie = "C:\\xampp\\htdocs\\Git\\get_toctoc\\cookieTT.txt";
        // $cookie = '/home/rriveros/public_html/get_geoplaceit/cookieGP.txt';

        //especificamos el POST (tambien podemos hacer peticiones enviando datos por GET
        curl_setopt ($ch2, CURLOPT_POST, false);

        //le decimos que queremos recoger una respuesta (si no esperas respuesta, ponlo a false)
        curl_setopt($ch2,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch2, CURLOPT_COOKIEFILE, $cookie);
        curl_setopt($ch2, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch2, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:72.0) Gecko/20100101 Firefox/72.0");

        //recogemos la respuesta
        $respuesta2 = curl_exec($ch2);

        curl_close($ch2);

        $respuesta2 = (array)json_decode($respuesta2);

        $datosIngreso = array();

        $datosIngreso['id'] = 'GEO' . $respuesta2['id'];
        // $datosIngreso['urlFicha'] =
        $datosIngreso['observaciones'] = str_replace("'","/", $respuesta2['descripcion']);
        $datosIngreso['observaciones'] = eliminar_acentos($datosIngreso['observaciones']);
        $datosIngreso['mtsUtiles'] = 0;
        $datosIngreso['mtsTerraza'] = 0;
        $datosIngreso['mtsContruidos'] = $respuesta2['dimension_propiedad'];
        if($datosIngreso['mtsContruidos'] == ''){
          $datosIngreso['mtsContruidos'] = 0;
        }
        $datosIngreso['mtsTerreno'] = $respuesta2['dimension_terreno'];
        if($datosIngreso['mtsTerreno'] == ''){
          $datosIngreso['mtsTerreno'] = 0;
        }
        $datosIngreso['dormitorios'] = $respuesta2['habitaciones'];
        if($datosIngreso['dormitorios'] == ''){
          $datosIngreso['dormitorios'] = 0;
        }
        $datosIngreso['banos'] = $respuesta2['banos'];
        if($datosIngreso['banos'] == ''){
          $datosIngreso['banos'] = 0;
        }
        if(is_null($respuesta2['por_dueno'])){
          $datosIngreso['tipoCliente'] = 1;
          $datosIngreso['contactoTipo'] = 1;
          $datosIngreso['nombreCorredora'] = '';
          $datosIngreso['corredora'] = 0;
        }
        else{
          $datosIngreso['tipoCliente'] = 2;
          $datosIngreso['contactoTipo'] = 2;
          $datosIngreso['nombreCorredora'] = $respuesta2['nombre_contacto'];
          $datosIngreso['corredora'] = 1;
        }
        $datosIngreso['publicada'] = 1;
        $datosIngreso['eliminada'] = 0;

        /* Valor de la uf desde el api */
        $ch = curl_init();
    		$fecha = new Datetime();
    		$fecha = $fecha->format('d-m-Y');
    		curl_setopt($ch, CURLOPT_URL, "https://generico.cryptodata.cl/uf_api/valorUFDia.php?fecha=" . $fecha);
       	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $uf = floatval(str_replace(",",".",str_replace(".","",curl_exec($ch))));
        /* Fin valor uf */

        if($respuesta2['currency']->simbolo_moneda == 'UF'){
          $datosIngreso['valorUF'] = $respuesta2['precio'];
          $datosIngreso['valorPesos'] = $respuesta2['precio']*$uf;
        }
        else{
          $datosIngreso['valorUF'] = $respuesta2['precio']/$uf;
          $datosIngreso['valorPesos'] = $respuesta2['precio'];
        }
        if($datosIngreso['valorUF'] == ''){
          $datosIngreso['valorUF'] = 0;
        }
        if($datosIngreso['valorPesos'] == ''){
          $datosIngreso['valorPesos'] = 0;
        }
        $datosIngreso['despliegue'] = $respuesta2['currency']->simbolo_moneda;
        $datosIngreso['direccionCalle'] = '';
        $datosIngreso['direccionNumero'] = '';
        $datosIngreso['direccionDepto'] = '';
        $datosIngreso['direccionComuna'] = $respuesta2['commune']->nombre;
        $datosIngreso['direccionRegion'] = $respuesta2['region']->nombre;
        $datosIngreso['tipoOperacion'] = $respuesta2['mode']->nombre;
        $datosIngreso['latitud'] = $respuesta2['ubicacion']->lat;
        $datosIngreso['longitud'] = $respuesta2['ubicacion']->lng;
        $datosIngreso['contactoNombre'] = '';
        $datosIngreso['contactoEmail'] = '';
        $datosIngreso['contactoTelefono'] = '';
        $fechaCrea = new Datetime($respuesta2['created_at']);
    		$fechaCrea = $fechaCrea->format('Y-m-d');
        $datosIngreso['nuevafecha'] = $fechaCrea;
        $datosIngreso['habitacional'] = 1;
        if($respuesta2['type']->nombre == "Casa"){
          $datosIngreso['tipoPropiedad'] = 1;
        }
        else{
          $datosIngreso['tipoPropiedad'] = 2;
        }
        if($respuesta2['mode']->nombre == "Venta"){
          $datosIngreso['urlFicha'] = strtolower('https://www.goplaceit.com/cl/propiedad/venta/' . $respuesta2['type']->nombre . '/' . str_replace(" ","-",$respuesta2['commune']->nombre) . '/' . $respuesta2['id'] . '-en-venta');
        }
        else{
          $datosIngreso['urlFicha'] = strtolower('https://www.goplaceit.com/cl/propiedad/arriendo/' . $respuesta2['type']->nombre . '/' . str_replace(" ","-",$respuesta2['commune']->nombre) . '/'  . $respuesta2['id'] . '-en-arriendo');
        }

        if($datosIngreso['despliegue'] == "UF"){
          $datosIngreso['idTipoMoneda'] = 2;
          $datosIngreso['precioDespliegue'] = $datosIngreso['valorUF'];
        }
        else{
          $datosIngreso['idTipoMoneda'] = 1;
          $datosIngreso['precioDespliegue'] = $datosIngreso['valorPesos'];
        }

        $fecha = date('Y-m-d H:i:s');

        $inserta = ingresaInmueble($datosIngreso['publicada'],$datosIngreso['eliminada'],$datosIngreso['habitacional'],$datosIngreso['nuevafecha'],$datosIngreso['mtsUtiles'],$datosIngreso['mtsTerraza'],$datosIngreso['mtsContruidos'],$datosIngreso['mtsTerreno'],$datosIngreso['dormitorios'],$datosIngreso['banos'],$datosIngreso['valorUF'],$datosIngreso['valorPesos'],'0',$datosIngreso['tipoCliente'],$datosIngreso['id'],2,$datosIngreso['idTipoMoneda'],$datosIngreso['contactoTipo'],$datosIngreso['urlFicha'],$datosIngreso['observaciones'],'',$datosIngreso['nombreCorredora'],$datosIngreso['precioDespliegue'],'',$datosIngreso['direccionCalle'],$datosIngreso['direccionDepto'],$datosIngreso['direccionNumero'],$datosIngreso['direccionComuna'],$datosIngreso['direccionRegion'],$datosIngreso['tipoOperacion'],$datosIngreso['contactoNombre'],$datosIngreso['contactoEmail'],$datosIngreso['contactoTelefono'],$datosIngreso['tipoPropiedad'],$fecha);

        echo $inserta . " - " . $datosIngreso['id'] . "\n";
      }
      else{
        echo "Ya existe" . " - " . 'GEO' . $respuesta[$j] . "\n";
      }
    }
  }

  function eliminar_acentos($cadena){
		//Reemplazamos la A y a
		$cadena = str_replace(
		array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
		array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
		$cadena
		);

		//Reemplazamos la E y e
		$cadena = str_replace(
		array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
		array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
		$cadena );

		//Reemplazamos la I y i
		$cadena = str_replace(
		array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
		array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
		$cadena );

		//Reemplazamos la O y o
		$cadena = str_replace(
		array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
		array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
		$cadena );

		//Reemplazamos la U y u
		$cadena = str_replace(
		array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
		array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
		$cadena );

		//Reemplazamos la N, n, C y c
		$cadena = str_replace(
		array('Ñ', 'ñ', 'Ç', 'ç'),
		array('N', 'n', 'C', 'c'),
		$cadena
		);

		return $cadena;
	}

?>
